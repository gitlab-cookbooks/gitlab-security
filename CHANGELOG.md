gitlab-security CHANGELOG
=========================

This file is used to list changes made in each version of the gitlab-security cookbook.

0.1.6
-----
- Brian Neel - Enable OSSEC

0.1.3
-----
- Brian Neel - Enable auditd with basic settings

0.1.2
-----
- John Northrup - Allow whitelisting of files and directory in rkhunter configuration

0.1.1
-----
- Jeroen Nijhof - Update license

0.1.0
-----
- Jeroen Nijhof - Initial release of gitlab-security

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
