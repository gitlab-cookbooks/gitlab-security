require "spec_helper"

describe "gitlab-security::rkhunter" do
  context "default execution" do
    let(:chef_run) {
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    }

    it "installs khunter" do
      expect(chef_run).to install_package("rkhunter")
    end

    it "creates the tmp dir for rkhunter" do
      expect(chef_run).to create_directory("/var/lib/rkhunter/tmp").with(
        owner: "root",
        group: "root",
        mode: "0700"
      )
    end
  end
end
