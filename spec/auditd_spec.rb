require "spec_helper"

describe "gitlab-security::auditd" do
  context "default execution" do
    let(:chef_run) {
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    }

    it "installs auditd" do
      expect(chef_run).to install_package("auditd")
    end

    it "creates the log dir for auditd" do
      expect(chef_run).to create_directory("/var/log/audit").with(
        owner: "root",
        group: "syslog",
        mode: "0750"
      )
    end
  end
end
