#
# Cookbook Name:: gitlab-security
# Recipe:: rkhunter
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
if node['platform_family'] == 'rhel'
  include_recipe 'yum-epel::default'
  package 'perl-libwww-perl'
end

package 'rkhunter'

directory '/var/lib/rkhunter/tmp' do
  owner 'root'
  group 'root'
  mode '0700'
end

template "/etc/rkhunter.conf" do
  owner 'root'
  group 'root'
  mode "0600"
  source "rkhunter.conf.#{node['platform_family']}.erb"
  action :create
  variables(
      :whitelist_file => node['gitlab-security']['rkhunter']['whitelist_file'],
      :whitelist_dir => node['gitlab-security']['rkhunter']['whitelist_dir']
  )
  notifies :run, 'execute[rkhunter --propupd]'
end

execute 'rkhunter --propupd' do
  action :nothing
end
