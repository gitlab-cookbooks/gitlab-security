#
# Cookbook Name:: gitlab-security
# Recipe:: auditd
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#

if node['platform_family'] == 'rhel'
  package 'audit'
else
  package 'auditd'
end

if not node['gitlab-security']['kitchen']
  service 'auditd' do
    supports :restart => true, :reload => true
    action [:enable, :start]
  end
end

group 'syslog' do
  action :create
end

directory '/var/log/audit' do
  owner 'root'
  group 'syslog'
  mode '0750'
end

file '/etc/audit/rules.d/audit.rules' do
  action :delete
end

template "/etc/audit/auditd.conf" do
  owner 'root'
  group 'root'
  mode "0640"
  source "auditd.conf.erb"
  action :create
  variables(
      :log_file => node['gitlab-security']['auditd']['log_file'],
      :num_logs => node['gitlab-security']['auditd']['num_logs'],
      :max_log_file => node['gitlab-security']['auditd']['max_log_file'],
      :space_left => node['gitlab-security']['auditd']['space_left'],
      :admin_space_left => node['gitlab-security']['auditd']['admin_space_left']
  )
  notifies :restart, "service[auditd]" if not node['gitlab-security']['kitchen']
end

template "/etc/audit/audit.rules" do
  owner 'root'
  group 'root'
  mode "0640"
  source "audit.rules.erb"
  action :create
  variables(
      :max_event_buffer => node['gitlab-security']['auditd']['max_event_buffer']
  )
  notifies :restart, "service[auditd]" if not node['gitlab-security']['kitchen']
end
