# Ossec server provisioning recipe
# install the ossec-hids package and push the
# default configuration from the templates

package 'curl'
#package 'auditd'

groupname = "root"

if node['platform_family'] == "rhel"
#  include_recipe "yum-atomic"
elsif node['platform_family'] == "debian"
    groupname = "ossec"
    file '/etc/apt/sources.list.d/gitlab.list' do
      content "deb [arch=amd64] http://aptly.gitlab.com/ xenial main\n"
      notifies :run, 'bash[add key]', :immediately
  end

  bash 'add key' do
    code <<-EOH
      curl -s http://aptly.gitlab.com/release.asc | apt-key add -
      apt-get update
    EOH
    action :nothing
  end
end


class Chef::Recipe
  include OssecCore
end

if node['platform_family'] == "debian"
  package "ossec-hids"
end

# Get all the agents at once, more efficient
if Chef::Config[:solo]
  Chef::Log.warn('This recipe uses search. Chef Solo does not support search')
else
#  ossec_agents = search(:node,
#                        "roles:ossec-agent "\
#                        "AND chef_environment:#{node.chef_environment}")
  ossec_agents = {}

  # set local command/active-response flags
  ossec_set_filtered_flags!("command", "active-response", "syslog_files")
  ossec_set_syscheck_flags!("ignore")

  # resolve searches in server rules
  ossec_hostname_search()

  # resolve email_alerts location searches
#  ossec_event_location_search()

  # initialize the agent hash on the first run
  if node["ossec"]["agents"].nil?
    node.normal["ossec"]["agents"] = {}
  end
end

directory "/var/ossec" do
  mode			'0550'
  owner			"root"
  group			"#{groupname}"
end

directory "/var/ossec/etc" do
  mode 			'0550'
  owner			"root"
  group			"#{groupname}"			
end

directory "/var/ossec/rules" do
  mode 			'0550'
  owner                 "root"
  group                 "#{groupname}"
end

directory "/var/ossec/etc" do
  mode 			'0550'
  owner                 "root"
  group                 "#{groupname}"
end

directory "/var/ossec/bin" do
  mode			'0550'
  owner			"root"
  group			"#{groupname}"
end

template "/var/ossec/etc/client.keys" do
  mode			'0440'
  owner			"root"
  group			"#{groupname}"
end

template "/var/ossec/rules/local_rules.xml" do
  owner			"root"
  group			"#{groupname}"
  notifies :restart, "service[ossec-server]", :delayed
end

template "/var/ossec/etc/local_decoder.xml" do
  owner			"root"
  group			"#{groupname}"
  notifies		:restart, "service[ossec-server]", :delayed
end

template "/var/ossec/etc/ossec.conf" do
  source		"ossec-server.conf.erb"
  owner			"#{groupname}"
  group			"#{groupname}"
  manage_symlink_source	true
  variables(
			:ossec_agents => ossec_agents
  )
  notifies		:restart, "service[ossec-server]", :delayed
end

template "/var/ossec/etc/internal_options.conf" do
  mode			"0444"
  owner			"root"
  group			"#{groupname}"
  notifies		:restart, "service[ossec-server]", :delayed
end

template "/var/ossec/bin/.process_list" do
  mode			"0440"
  owner			"root"
  group			"#{groupname}"
  notifies		:restart, "service[ossec-server]", :delayed
end

service "ossec-server" do
  #provider Chef::Provider::Service::Init
  service_name		node["ossec"]["server"]["service_name"]
  supports		:start => true, :stop => true, :restart => true, :status => true
  action		[ :start ]
  only_if		{ File.exist?("/var/ossec/bin/ossec-control") }
end

