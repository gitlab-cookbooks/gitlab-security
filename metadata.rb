name             'gitlab-security'
maintainer       'GitLab Inc.'
maintainer_email 'jeroen@gitlab.com'
license          'MIT'
description      'Security cookbook for GitLab infra'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.10'

depends 'yum-epel'
