# gitlab-security Cookbook
This cookbook contains all security related recipes for our infrastructure.

## Usage
### gitlab-security::rkhunter
This recipe will install rkhunter

Just include `gitlab-security::rkhunter` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[gitlab-security::rkhunter]"
  ]
}
```

## Contributing
1. Fork the repository on GitLab
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Merge Request using GitLab

## License and Authors
Authors: Jeroen Nijhof <jeroen@gitlab.com>

License: MIT
