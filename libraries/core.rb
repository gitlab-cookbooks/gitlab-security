# Core function for OSSEC
# Used by the server and agent recipes

module OssecCore

  def ossec_hostname_search()
    # resolve the hostname_search of a rule to a list of hosts
    node["ossec"]["rules"].each do |id,params|
      if not params.nil?
        params[:body].each do |key, value|
          if key.eql?('hostname_search')
            hosts_list = search(:node,
                                "(#{value}) AND roles:ossec-agent "\
                                " AND chef_environment:#{node.chef_environment}"
                                ).map {|n| n.hostname}
            if hosts_list.empty?
              # search didn't return anything
              # store a dummy value in the attributes
              Chef::Log.info("OSSEC: Hostname search returned empty result. " +
                             "'#{value}'")
              params[:body][:hostname] = "invalid-search-returned-empty-result"
            else
              # store in the node params but discard the last char
              params[:body][:hostname] = hosts_list.join('|')
            end
          end
        end
      end
    end
  end


  def ossec_event_location_search()
    # resolve the location search of an email_alert block to a hostname
    node["ossec"]["email_alerts"].each do |recipient, params|
      if params.has_key?('event_location_search')
        if Chef::Config[:solo]
          Chef::Log.warn('This recipe uses search. Chef Solo does not support search.')
        else
          dest = search(:node,
                        "(#{params[:event_location_search]}) " \
                        "AND chef_environment:#{node.chef_environment}"
                       ).map {|n| n.hostname}
          node.default["ossec"]["email_alerts"][recipient]["resolved_search"] = dest
        end
      end
    end
  end

  def ossec_set_syscheck_flags!(*args)
    # go through the list of command/active-response and check the ones
    # that apply to this node
    args.each do |config|
      unless node["ossec"]["syscheck"][config].nil?
        node["ossec"]["syscheck"][config].each do |item, params|
          unless params[:apply_to].nil?
            locations = search(:node,
                               "(#{params[:apply_to]}) " \
                               "AND chef_environment:#{node.chef_environment}"
                              ).map {|n| n.ipaddress}
            if locations.include?(node["ipaddress"])
              node.default["ossec"]["syscheck"][config][item]["use_here"] = true
            else
              node.default["ossec"]["syscheck"][config][item]["use_here"] = false
            end
          else
            node.default["ossec"]["syscheck"][config][item]["use_here"] = true
          end
        end
      end
    end
  end

  def ossec_set_filtered_flags!(*args)
    # go through the list of command/active-response and check the ones
    # that apply to this node
    args.each do |config|
      unless node["ossec"][config].nil?
        node["ossec"][config].each do |item, params|
          unless params[:apply_to].nil?
            locations = search(:node,
                               "(#{params[:apply_to]}) " \
                               "AND chef_environment:#{node.chef_environment}"
                              ).map {|n| n.ipaddress}
            if locations.include?(node["ipaddress"])
              node.default["ossec"][config][item]["use_here"] = true
            else
              node.default["ossec"][config][item]["use_here"] = false
            end
          else
            node.default["ossec"][config][item]["use_here"] = true
          end
        end
      end
    end
  end
end
